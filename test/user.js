// During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let User = require('../models/user.js');

// Require the dev-dependencies
let app = require('../app');
let chai = require('chai');
let chaiHttp = require('chai-http');
let request = require('supertest');

// Set up test agent
let agent = request.agent(app);

chai.use(chaiHttp);

// Use test object to add, get, and delete single node
let testUser = { email: 'bob.peterson@example.com', first_name: 'Bob', last_name: 'Peterson' };
let testUserId = null;


/*
 * Sign in and acquire valid token
 */
let token = null;

describe('POST /api/sign-in', function(done){

  it('should return a 200 response if a valid user name and a valid password are provided', function(done){
    agent
      .post('/api/sign-in')
      .send({ email: 'alice.myer@example.com', password: 'test' })
      .end(function(err, res){
      if (err) return done(err);
        token = res.body.token;
        done();
      })
      .expect(200);
  });
});

  /*
   * Test the /GET route
   */
  describe('/GET users', () => {
    it('It should GET all the users', (done) => {
      chai.request(app)
      .get('/api/users')
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('array');
        res.body.length.should.be.eql(1);
        done();
      });
    });
  });


/*
 * Test the /POST route
 */
describe('/POST user', () => {

  it('It should not POST a user without email field', (done) => {
    let user = {
      first_name: 'Bob',
      last_name: 'Peterson' 
    }
    chai.request(app)
    .post('/api/users')
    .set('x-access-token', token)
    .send(user)
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('errors');
      res.body.errors.should.have.property('email');
      res.body.errors.email.should.have.property('kind').eql('required');
      done();
    });
  });

  it('It should POST a user', (done) => {
//    let user = new User ({
//      email:  'bob.peterson@example.com'
//    }); 

    chai.request(app)
    .post('/api/users')
    .set('x-access-token', token)
    .send(testUser)
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('message').eql('User successfully added!');
      res.body.user.should.have.property('email');
      testUserId = res.body.user._id;  
      done();
    });
  });
});


/*
 * Test the /GET/:id route
 */
describe('/GET/:id user', () => {

  it('It should not GET a user by a non-existing id', (done) => {
    chai.request(app)
    .get('/api/users/' + '000000000000000000000000')
    .end((err, res) => {
      res.should.have.status(404);
      res.body.should.be.a('object');
      res.body.should.have.property('error');
      done();
    });
  });

  it('It should GET a user by the given id', (done) => {
    chai.request(app)
    .get('/api/users/' + testUserId)
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('email');
      res.body.should.have.property('_id').eql(testUserId.toString());
      done();
    });
  });

});

 /*
  * TODO: 
  * Test the /PUT/:id route
  */

 /*
  * TODO: 
  * Test the /DELETE/:id route
  */


/*
 * Test the /POST route
 */
describe('POST /users/upload file', () => {
  it('It should upload the given file.', (done) => {
    request(app)
    .post('/api/users/upload')
    .set('x-access-token', token)
    .attach('file', 'test/data/users.json')
    .expect(200, {
      message: 'Users successfully uploaded.',
    }, done);
  });
});


/*
 * Test the /GET route
 */
describe('/GET users', function() {
  it('It should GET an array of users', (done) => {
    chai.request(app)
    .get('/api/users')
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('array');
      done();
    });
  });
});


/*
 * Test the /GET route
 */
describe('/GET users', function() {
  it('It should GET all uploaded users', (done) => {
    chai.request(app)
    .get('/api/users')
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('array');
      res.body.length.should.be.eql(2);
      done();
    });
  });
});

