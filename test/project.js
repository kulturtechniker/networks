// During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let Project = require('../models/project.js');
let User = require('../models/user.js');

// Require the dev-dependencies
let app = require('../app');
let chai = require('chai');
let chaiHttp = require('chai-http');
let request = require('supertest');

// Set up test agent
let agent = request.agent(app);

chai.use(chaiHttp);

// Use test object to add, get, and delete single node
let testProject = { title: "My first test project" };
let testProjectId = null;


/* 
 * Sign in and acquire valid token
 */
let token = null;

describe('POST /api/sign-in', function(done){

  it('should return a 200 response if a valid user name and a valid password are provided', function(done){
    agent
      .post('/api/sign-in')
      .send({ email: 'alice.myer@example.com', password: 'test' })
      .end(function(err, res){
      if (err) return done(err);
        token = res.body.token;
        done();
      })
      .expect(200);
  });
});



/*
 * Test the /POST route
 */
describe('/POST project', () => {

  it('It should not POST a project without a title field', (done) => {
    let project = {
      description: 'This is the description of the first project' 
    }
    chai.request(app)
    .post('/api/projects')
    .set('x-access-token', token)
    .send(project)
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('errors');
      res.body.errors.should.have.property('title');
      res.body.errors.title.should.have.property('kind').eql('required');
      done();
    });
  });

  it('It should not POST a project with an empty title field', (done) => {
    let project = {
      title: '',
      description: 'This is the description of the first project' 
    }
    chai.request(app)
    .post('/api/projects')
    .set('x-access-token', token)
    .send(project)
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('errors');
      res.body.errors.should.have.property('title');
      res.body.errors.title.should.have.property('kind').eql('required');
      done();
    });
  });

  it('It should POST a project', (done) => {
    let project = {
      title: 'The first project', 
      description: 'This is the description of the first project' 
    }
    chai.request(app)
    .post('/api/projects')
    .set('x-access-token', token)
    .send(project)
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.project.should.have.property('title');
      res.body.project.should.have.property('description');
      res.body.should.have.property('message').eql('Project successfully added!');
      testProjectId = res.body.project._id;
      done();
    });
  });

}); 


/*
 * Test the /GET/:id route
 */
describe('/GET/:id project', () => {

  it('It should not GET a project by a non-existing id', (done) => {
    chai.request(app)
    .get('/api/projects/' + '000000000000000000000000')
    .set('x-access-token', token)
    .end((err, res) => {
      res.should.have.status(404);
      res.body.should.be.a('object');
      res.body.should.have.property('error');
      done();
    });
  });

  it('It should GET a project by the given id', (done) => {
    chai.request(app)
    .get('/api/projects/' + testProjectId)
    .set('x-access-token', token)
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('title');
      res.body.should.have.property('description');
      res.body.should.have.property('user');
      res.body.should.have.property('_id').eql(testProjectId.toString());
      done();
    });
  });

});


/*
 * Test the /PUT/:id route
 */
describe('/PUT/:id project', () => {
  it('It should UPDATE a project given the id', (done) => {
    chai.request(app)
      .put('/api/projects/' + testProjectId)
      .set('x-access-token', token)
      .send({title: 'My first project modified', description: 'This is a modification of my first project'})
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('message').eql('Project updated!');
        res.body.project.should.have.property('title').eql('My first project modified');
        res.body.project.should.have.property('description').eql('This is a modification of my first project');
        done();
      });
  });
});


/*
 * Test the /DELETE/:id route
 */
describe('/DELETE/:id project', () => {
  it('It should DELETE a project given the id', (done) => {

    chai.request(app)
      .delete('/api/projects/' + testProjectId)
      .set('x-access-token', token)
      .end((err, res) => {

        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('message').eql('Project successfully deleted!');
        res.body.project.should.have.property('title').eql('My first project modified');
        res.body.project.should.have.property('description').eql('This is a modification of my first project');
        done();
      });
  });
});


/*
 * Test the /POST route
 */
describe('POST /projects/upload file', () => {
  it('It should upload the given file.', (done) => {

    request(app)
      .post('/api/projects/upload')
      .set('x-access-token', token)
      .attach('file', 'test/data/projects.json')
      .expect(200, {
        message: 'Projects successfully uploaded.',
      }, done);
  });
});


/*
 * Test the /GET route
 */
describe('/GET projects', function() {
  it('It should GET an array of projects', (done) => {
    chai.request(app)
    .get('/api/projects')
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('array');
      done();
    });
  });
});


/*
 * Test the /GET route
 */
describe('/GET projects', function() {
  it('It should GET all uploaded projects', (done) => {
    chai.request(app)
    .get('/api/projects')
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('array');
      res.body.length.should.be.eql(2);
      done();
    });
  });
});

