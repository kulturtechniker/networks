// During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let Node = require('../models/node.js');
let User = require('../models/user.js');

// Require the dev-dependencies
let app = require('../app');
let chai = require('chai');
let chaiHttp = require('chai-http');
let request = require('supertest');

// Set up test agent
let agent = request.agent(app);

chai.use(chaiHttp);

// testProject required to add a single node
let testProject = { title: "My first project" }; 
let testProjectId = null;

// Use test object to add, get, and delete single node
let testNode = { id: "foo", group: 8 }; 
let testNodeId = null;

/*
 * Sign in and acquire valid token
 */
let token = null;

describe('POST /api/sign-in', function(done){

  it('should return a 200 response if a valid user name and a valid password are provided', function(done){
    agent
      .post('/api/sign-in')
      .send({ email: 'alice.myer@example.com', password: 'test' })
      .end(function(err, res){
      if (err) return done(err);
        token = res.body.token;
        done();
      })
      .expect(200);
  });
});


/*
 * Test the /POST route
 */
describe('/POST node', () => {

  it('It should POST a project', (done) => {
    let project = {
      title: 'The first project',
      description: 'This is the description of the first project'
    }
    chai.request(app)
    .post('/api/projects')
    .set('x-access-token', token)
    .send(project)
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.project.should.have.property('title');
      res.body.project.should.have.property('description');
      res.body.should.have.property('message').eql('Project successfully added!');
      testProjectId = res.body.project._id;
      done();
    });
  });

  it('It should not POST a node without id field', (done) => {
    let node = {
      group: 5,
      project: testProjectId
    }
    chai.request(app)
    .post('/api/nodes')
    .set('x-access-token', token)
    .send(node)
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('errors');
      res.body.errors.should.have.property('id');
      res.body.errors.id.should.have.property('kind').eql('required');
      done();
    });
  });

  it('It should not POST a node without project field', (done) => {
    let node = {
      group: 5,
      id: 'My first node'
    }
    chai.request(app)
    .post('/api/nodes')
    .set('x-access-token', token)
    .send(node)
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('errors');
      res.body.errors.should.have.property('project');
      res.body.errors.project.should.have.property('kind').eql('required');
      done();
    });
  });

  it('It should POST a node', (done) => {

    // Update testNode with valid projectId
    testNode.project = testProjectId; 

    chai.request(app)
    .post('/api/nodes')
    .set('x-access-token', token)
    .send(testNode)
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('message').eql('Node successfully added!');
      res.body.node.should.have.property('id');
      res.body.node.should.have.property('group');
      testNodeId = res.body.node._id; 
      done();
    });
  });
});


/*
 * Test the /GET/:id route
 */
describe('/GET/:id node', () => {

  it('It should not GET a node by a non-existing id', (done) => {
    chai.request(app)
    .get('/api/nodes/' + '000000000000000000000000')
    .end((err, res) => {
      res.should.have.status(404);
      res.body.should.be.a('object');
      res.body.should.have.property('error');
      done();
    });
  });

  it('It should GET a node by the given id', (done) => {
    chai.request(app)
    .get('/api/nodes/' + testNodeId)
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('id').eql(testNode.id);
      res.body.should.have.property('group').eql(testNode.group);
      res.body.should.have.property('user');
      res.body.should.have.property('_id').eql(testNodeId.toString());
      done();
    });
  });

});


/*
 * Test the /PUT/:id route
 */
describe('/PUT/:id node', () => {
  it('It should UPDATE a node given the id', (done) => {
    chai.request(app)
      .put('/api/nodes/' + testNodeId)
      .set('x-access-token', token)
      .send({id: "bar", group: 5})
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('message').eql('Node updated!');
        res.body.node.should.have.property('id').eql('bar');
        res.body.node.should.have.property('group').eql(5);
        done();
      });
  });
});


/*
 * Test the /DELETE/:id route
 */
describe('/DELETE/:id node', () => {
  it('It should DELETE a node given the id', (done) => {

    chai.request(app)
      .delete('/api/nodes/' + testNodeId)
      .set('x-access-token', token)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('message').eql('Node successfully deleted!');
        res.body.node.should.have.property('id').eql('bar');
        res.body.node.should.have.property('_id').eql(testNodeId);
        done();
      });
  });
});

/*
 * Remove the test project  
 */
describe('Remove test project', () => {
  it('Remove the test project', (done) => {

    chai.request(app)
      .delete('/api/projects/' + testProjectId)
      .set('x-access-token', token)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('message').eql('Project successfully deleted!');
        res.body.project.should.have.property('_id').eql(testProjectId);
        done();
      });
  });
});


/*
 * Test the /POST route
 */
describe('POST /nodes/upload file', () => {
  it('It should upload the given file.', (done) => {

  request(app)
    .post('/api/nodes/upload')
    .set('x-access-token', token)
    .attach('file', 'test/data/nodes.json')
    .expect(200, {
      message: 'Nodes successfully uploaded.',
    }, done);
  });
});


/*
 * Test the /GET route
 */
describe('/GET nodes', () => {
  it('It should GET an array of nodes', (done) => {
    chai.request(app)
    .get('/api/nodes')
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('array');
      done();
    });
  });
});

/*
 * Test the /GET route
 */
describe('/GET nodes', () => {
  it('It should GET all uploaded nodes', (done) => {
    chai.request(app)
    .get('/api/nodes')
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('array');
      res.body.length.should.be.eql(75);
      done();
    });
  });
});

