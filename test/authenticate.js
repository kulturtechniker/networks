// Require the dev-dependencies
process.env.NODE_ENV = 'test' 

let app = require('../app');
let expect = require('chai').expect;
let request = require('supertest');

let Link = require('../models/link.js');
let Node = require('../models/node.js');
let Project = require('../models/project.js');
let User = require('../models/user.js');

let agent = request.agent(app);
let token = null; 


before((done) => { 

  console.log('Remove any links before any test in any file');

  Link.deleteMany({}, (err) => { done() });     
    
});

before((done) => { 

  console.log('Remove any nodes before any test in any file');

  Node.deleteMany({}, (err) => { done() });     
    
});

before((done) => { 

  console.log('Remove any project before any test in any file');

  Project.deleteMany({}, (err) => { done() });     
    
});

before((done) => { 

  console.log('Remove any users before any test in any file');
  console.log('');
  console.log('');

  User.deleteMany({}, (err) => { done() });     
    
});


describe('POST /api/sign-up', function(done){
  
  it('should return a 200 response if a valid user name and two matching passwords are provided', function(done){
    agent
      .post('/api/sign-up')
      .send({ email: 'alice.myer@example.com', password: 'test', passwordConfirmation: 'test' })
      .end(function(err, res){
        if (err) return done(err);
        token = res.body.token;
        done();
      })
      .expect(200);
  });

  it('should return a 500 response if the user name already exists', function(done){
    agent
      .post('/api/sign-up')
      .send({ email: 'alice.myer@example.com', password: 'test', passwordConfirmation: 'test' })
      .expect(500, done);
  });

  it('should return a 500 response if the password is missing or empty', function(done){
    agent
      .post('/api/sign-up')
      .send({ email: 'bob.peterson@example.com',  passwordConfirmation: 'test' })
      .expect(500, done);
  });

  it('should return a 500 response if the passwordConfirmation is missing or empty', function(done){
    agent
      .post('/api/sign-up')
      .send({ email: 'bob.peterson@example.com',  password: 'test' })
      .expect(500, done);
  });

  it('should return a 500 response if password and passwordConfirmation do not match', function(done){
    agent
      .post('/api/sign-up')
      .send({ email: 'bob.peterson@example.com',  password: 'test', passwordConfirmation: 'no match' })
      .expect(500, done);
  });

}); 

describe('GET /api/sign-out', function(done){

  it('should return a 200 response if the user has successfully been signed out', function(done){

    request(app)
      .get('/api/sign-out')
      .expect(200, done);     
  });

}); 

describe('POST /api/sign-in', function(done){
  
  it('should return a 200 response if a valid user name and a valid password are provided', function(done){
    agent
      .post('/api/sign-in')
      .send({ email: 'alice.myer@example.com', password: 'test' })
      .end(function(err, res){
        if (err) return done(err);
        token = res.body.token;
        done();
      })
      .expect(200);
  });

}); 

describe('GET /api/account', function(done) {

  it('should return a 200 response if a valid token is provided', function(done){

    request(app)
      .get('/api/account')
      .set('x-access-token', token)
      .expect('Content-Type', /json/)
      .expect(200, done);
  });

  it('should return a 403 response if no token is provided' , function(done){
    request(app)
      .get('/api/account')
      .expect('Content-Type', /json/)
      .expect(403, done);
  });

  it('should return a 500 response if the token provided is invalid' , function(done){
    request(app)
      .get('/api/account')
      .set('x-access-token', 'invalid-token')
      .expect('Content-Type', /json/)
      .expect(500, done);
  });
});

