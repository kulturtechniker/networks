// During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let Link = require('../models/link.js');
let Node = require('../models/node.js');
let User = require('../models/user.js');

// Require the dev-dependencies
let agent = null; 
let app = require('../app');
let chai = require('chai');
let chaiHttp = require('chai-http');
let request = require('supertest');
let should = chai.should();

// Set up test agent
agent = request.agent(app);

chai.use(chaiHttp);

// Use a test object to add, get, and delete single link 
let testLink = { id: "foo", group: 8 };
let testLinkId = null;

// testProject required to add nodes 
let testProjectId = null;

// Test nodes required to test adding and updating a link
let sourceNodeId = null; 
let targetNodeId = null; 

/*
 * Sign in and acquire valid token
 */
let token = null;

describe('POST /api/sign-in', function(done){

  it('should return a 200 response if a valid user name and a valid password are provided', function(done){
    agent
      .post('/api/sign-in')
      .send({ email: 'alice.myer@example.com', password: 'test' })
      .end(function(err, res){
      if (err) return done(err);
        token = res.body.token;
        done();
      })
      .expect(200);
  });
}); 

/*
 * Test the /POST route
 */
describe('/POST link', () => {

  it('It should POST a project', (done) => {
    let project = {
      title: 'The link project',
      description: 'This is the description of the link project'
    }
    chai.request(app)
    .post('/api/projects')
    .set('x-access-token', token)
    .send(project)
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.project.should.have.property('title');
      res.body.project.should.have.property('description');
      res.body.should.have.property('message').eql('Project successfully added!');
      testProjectId = res.body.project._id;
      done();
    });
  });

  it('It should POST a source node', (done) => {
    let node = {
      id: 'The source node',
      project: testProjectId
    }
    chai.request(app)
    .post('/api/nodes')
    .set('x-access-token', token)
    .send(node)
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('message').eql('Node successfully added!');
      res.body.node.should.have.property('id');
      sourceNodeId = res.body.node._id;
      done();
    });
  });

  it('It should POST a target node', (done) => {
    let node = {
      id: 'The target node',
      project: testProjectId
    }
    chai.request(app)
    .post('/api/nodes')
    .set('x-access-token', token)
    .send(node)
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('message').eql('Node successfully added!');
      res.body.node.should.have.property('id');
      targetNodeId = res.body.node._id;
      done();
    });
  });

  it('It should not POST a link without source field', (done) => {

    let link = {
      target: "bar"
    }

    chai.request(app)
      .post('/api/links')
      .set('x-access-token', token)
      .send(link)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('errors');
        res.body.errors.should.have.property('source');
        res.body.errors.source.should.have.property('kind').eql('required');
        done();
      });
  });

  it('It should not POST a link without target field', (done) => {

      let link = {
        source: "foo"
      }

      chai.request(app)
        .post('/api/links')
        .set('x-access-token', token)
        .send(link)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.errors.should.have.property('target');
          res.body.errors.target.should.have.property('kind').eql('required');
          done();
        });
    });

    it('It should not POST a link without a token', (done) => {

      let link = {
        source: sourceNodeId,
        target: targetNodeId
      }

      chai.request(app)
        .post('/api/links')
        .send(link)
        .end((err, res) => {
          res.should.have.status(403);
          res.body.should.be.a('object');
          res.body.should.have.property('message').eql('No token provided.');
          done();
      });
    });

    it('It should not POST a link with an invalid token', (done) => {

      let link = {
        source: sourceNodeId,
        target: targetNodeId
      }

      chai.request(app)
        .post('/api/links')
        .set('x-access-token', 'invalid-token')
        .send(link)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('message').eql('Failed to authenticate token.');
          done();
      });
    });

    // TODO
    it('It should not POST a link with the same source and target node'); 

    // TODO
    it('It should not POST a link with a non-existing source node'); 

    // TODO
    it('It should not POST a link with a non-existing target node'); 

    // TODO
    it('It should not POST a link with an invalid source node'); 

    // TODO
    it('It should not POST a link with an invalid target node'); 

    it('It should POST a link', (done) => {

      let link = {
        source: sourceNodeId,
        target: targetNodeId
      }

      chai.request(app)
      .post('/api/links')
      .set('x-access-token', token)
      .send(link)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('message').eql('Link successfully added!');
        res.body.link.should.have.property('source');
        res.body.link.should.have.property('target');
        res.body.link.should.have.property('value');
        testLinkId = res.body.link._id;
        done();
      });
    });
  });


/*
 * Test the /GET/:id route
 */
describe('/GET/:id link', () => {

  it('It should not GET a link by a non-existing id', (done) => {
    chai.request(app)
    .get('/api/links/' + '000000000000000000000000')
    .set('x-access-token', token)
    .end((err, res) => {
      res.should.have.status(404);
      res.body.should.be.a('object');
      res.body.should.have.property('error');
      done();
    });
  });

  it('It should GET a link by the given id', (done) => {
    chai.request(app)
    .get('/api/links/' + testLinkId)
    .set('x-access-token', token)
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('source');
      res.body.should.have.property('target');
      res.body.should.have.property('value');
      res.body.should.have.property('_id').eql(testLinkId.toString());
      done();
    });
  });

});

/*
 * Test the /PUT/:id route
 */
describe('/PUT/:id link', () => {

  it('it should UPDATE a link given the id', (done) => {
      
    chai.request(app)
      .put('/api/links/' + testLinkId)
      .set('x-access-token', token)
      .send({targetNodeId, sourceNodeId})
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('message').eql('Link updated!');
        res.body.link.should.have.property('source').eql(sourceNodeId.toString());
        res.body.link.should.have.property('target').eql(targetNodeId.toString());
        done();
      });
  });
});


/*
 * Test the /DELETE/:id route
 */
describe('/DELETE/:id link', () => {

  // TODO
  it('It should not DELETE a link with an invalid id.'); 

  // TODO
  it('It should not DELETE a link with a non-existing id.'); 

  it('It should DELETE a link given the id', (done) => {
    chai.request(app)
      .delete('/api/links/' + testLinkId)
      .set('x-access-token', token)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('message').eql('Link successfully deleted!');
        res.body.link.should.have.property('_id').eql(testLinkId.toString());
        done();
      });
  });
});


/*
 * Clean up  
 */
describe('Clean up', () => {

  it('Remove the source node', (done) => {
    chai.request(app)
      .delete('/api/nodes/' + sourceNodeId)
      .set('x-access-token', token)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('message').eql('Node successfully deleted!');
        res.body.node.should.have.property('_id').eql(sourceNodeId);
        done();
      });
  });

  it('Remove the target node', (done) => {
    chai.request(app)
      .delete('/api/nodes/' + targetNodeId)
      .set('x-access-token', token)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('message').eql('Node successfully deleted!');
        res.body.node.should.have.property('_id').eql(targetNodeId);
        done();
      });
  });

  it('Remove the test project', (done) => {
    chai.request(app)
      .delete('/api/projects/' + testProjectId)
      .set('x-access-token', token)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('message').eql('Project successfully deleted!');
        res.body.project.should.have.property('_id').eql(testProjectId);
        done();
      });
  });

});


/*
 * Test the /POST route
 */
describe('POST /links/upload file', () => {
  it('It should upload the given file.', (done) => {

    request(app)
      .post('/api/links/upload')
      .set('x-access-token', token)
      .attach('file', 'test/data/links.json')
      .expect(200, {
        message: 'Links successfully uploaded.',
      }, done);
   });
});


/*
 * Test the /GET route
 */
describe('/GET links', function() {
  it('It should GET an array of links', (done) => {
    chai.request(app)
    .get('/api/links')
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('array');
      done();
    });
  });
});


/*
 * Test the /GET route
 */
describe('/GET links', function() {
  it('It should GET an array of links', (done) => {
    chai.request(app)
    .get('/api/links')
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('array');
      res.body.length.should.be.eql(20);
      done();
    });
  });
});

