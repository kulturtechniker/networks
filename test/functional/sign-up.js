// Set the test environment to 'test'
process.env.NODE_ENV = 'test';

// get the application server module
// const app = require('../../app');

const Link = require('../../models/link.js');

describe('sign-up page', function() {

  it('should show a sign-up form');
  it('should refuse sign-up without email');
  it('should refuse invalid emails');
  it('should refuse sign-up without matching passwords');
  it('should keep values on partial sign-ups');
  it('should accept complete sign-up');

});
