const jwt = require('jsonwebtoken');
const config = require('config');

function getUser(req, res, next) {

  let token = req.cookies.token;

//  if (!token)
//    return res.status(403).send({ auth: false, message: 'No token provided.' });

  if (token) {
    jwt.verify(token, config.SECRET, function(err, decoded) {

      if (err)
        return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });

      // if everything good, save to request for use in other routes
      let user = { _id: decoded.id, email: decoded.email, full_name: decoded.full_name }; 

      req.user = user; 

    });
  }
  next();
}

module.exports = getUser;

