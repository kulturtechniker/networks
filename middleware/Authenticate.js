const jwt = require('jsonwebtoken');
const config = require('config');

function authenticate(req, res, next) {

  let token = req.cookies.token;

  if (!token)
    return res.redirect('/sign-in?redirect=' + req.originalUrl); 
//    return res.status(403).send({ auth: false, message: 'No token provided.' });

  jwt.verify(token, config.SECRET, function(err, decoded) {

    if (err)
      return res.redirect('/sign-in?redirect=' + req.originalUrl); 
//      return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });

    // if everything good, save to request for use in other routes
//    req.userId = decoded.id;
    next();
  });
}

module.exports = authenticate;

