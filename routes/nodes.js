const express = require('express');
const multer = require('multer');
const router = express.Router();

// Require controller modules.
const node_controller = require('../controllers/nodeController');
const Authenticate = require('../middleware/Authenticate');
const GetUser = require('../middleware/GetUser');

// Store uploaded files in memory
const storage = multer.memoryStorage();
const upload = multer({storage: storage});

/// NODE ROUTES ///

// GET request for list of all Nodes.
router.get('/', GetUser, node_controller.node_list);

// GET request for creating Node.
router.get('/create', GetUser, Authenticate, node_controller.createNodeGet);

// POST request for creating Node.
router.post('/create', GetUser, Authenticate, node_controller.createNodePost);

// GET request for uploading Nodes.
router.get('/upload', GetUser, Authenticate, node_controller.uploadNodesGet);

// POST request for uploading Nodes.
router.post('/upload', upload.single('file'), GetUser, Authenticate, node_controller.uploadNodesPost);

// GET request for one Node.
router.get('/:id', GetUser, node_controller.getNode); 

// POST request to update Node.
router.post('/:id', GetUser, Authenticate, node_controller.updateNode);

// GET request to delete Node.
router.get('/:id/delete', GetUser, Authenticate, node_controller.node_delete_get);

// POST request to delete Node.
router.post('/:id/delete', GetUser, Authenticate, node_controller.node_delete_post);

module.exports = router;

