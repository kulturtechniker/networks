const config = require('config');
const express = require('express');
const jwt = require('jsonwebtoken');
const multer = require('multer');
const objectId = require('mongoose').Types.ObjectId;
const passport = require("passport");
const router = express.Router();

// Require schema modules
const Link = require('../models/link');
const Node = require('../models/node');
const Project = require('../models/project');
const User = require('../models/user');

const VerifyToken = require('../middleware/VerifyToken');

// Store uploaded files in memory
const storage = multer.memoryStorage();
const upload = multer({storage: storage});


//
// AUTH API
//
router.get('/', function(req, res) {
  res.status(200).json({ message: 'This is the API of networks.' });
});

router.get('/account', VerifyToken, function(req, res, next) {

    User.findById(req.userId, {}, function (err, user) {
      if (err) return res.status(500).send("There was a problem finding the user.");
      if (!user) return res.status(404).send("No user found.");

      res.status(200).send(user);

    });
});

router.post('/sign-in', function(req, res) {

  User.findOne({ email: req.body.email }, {} , function (err, user) {

    if (err) return res.status(500).send('Error on the server.');
    if (!user) return res.status(404).send('No user found.');

    passport.authenticate('local', { session: false })(req, res, function () {

      // create a token
      var token = jwt.sign({ id: user._id, email: user.email, full_name: user.full_name }, config.SECRET, {
        expiresIn: 86400 // expires in 24 hours
      });

      res.status(200)
        .send({ auth: true, token: token, user: user });

    });
  });
});

router.get('/sign-out', function(req, res) {
  res.status(200).send({ auth: false, token: null });
});

router.post('/sign-up', function(req, res) {
  
  if (!req.body.password) {
    return res.status(500).send("Password is required.");
  }

  if (!req.body.passwordConfirmation) {
    return res.status(500).send("The password has to be confirmed.");
  }

  if (req.body.password.localeCompare(req.body.passwordConfirmation) != 0) {
    return res.status(500).send("The two passwords do no match.");
  }

  User.register(new User({ email: req.body.email, password: req.body.password}), req.body.password, function(err, user) {

    if (err) {
      return res.status(500).send("There was a problem registering the user.");
    }

    passport.authenticate('local', { session: false })(req, res, function () {

      // create a token
      var token = jwt.sign({ id: user._id }, config.SECRET, {
        expiresIn: 86400 // expires in 24 hours
      });

      res.status(200)
        .cookie('token', token, { maxAge: 86400 })
        .send({ auth: true, token: token, user: user });

    });
  });
});

// 
// LINK API 
//
// GET request for list of all Links.
router.get('/links', function (req, res) {

  // Common parameters, both for paginated
  // and unpaginated results
  let joins = []; 
  let node = req.query.node;
  let populate = 'true' === req.query.populate;
  let query = {};
  let sort = req.query.sort;
  let sorts = { modifiedDate: 'desc'}; 

  if (sort) {
    // multiple sorts are comma separated
    let tokens = sort.split(',');
    console.log('sort = ' + sort); 
    console.log('tokens.length = ' + tokens.length); 
  }

  if (node) {
    query = { $or: [ {source: node}, {target: node} ] };
  }

  if (populate) joins = ['source','target','user'];


  if (req.query.page) {     

    let limit = 10;  
    let page = 1; 

    if (parseInt(req.query.page, 10)) {
      page = parseInt(req.query.page, 10);
      if (page < 0) { page = 1; }
    };

    // Return paginated results
    Link
      .paginate(query, { page: page, limit: limit, populate: joins, sort: sorts })
      .then((err, response) => {
        if(err) res.send(err);
        res.json(response);
      });

  } else {                  

    // Return unpaginated results
    Link 
      .find(query, '')
      .sort(sorts)
      .populate(joins)
      .then((err, response) => {
        if(err) res.send(err);
        res.json(response);
      });
  }
});

// GET request for a specific Link.
router.get('/links/:id', function (req, res) {

  if (objectId.isValid(req.params.id)) {
    Link.findById(req.params.id)
    .populate(['user'])
    .then(function(err,link) {
      if (err) {
        res.send(err)
      }
      if (link === undefined) {
        try {
          res.status(404).send( {'error': 'The requested link does not exist.'} )
        } catch (e) {
        }
      } else {
        res.json(link)
      }
    });
  } else {
    res.status(404).send( {'error': 'The requested link does not exist.'} )
  }

});


// POST request to create new Link
router.post('/links', VerifyToken, function (req, res) {

  // Create a new link
  let newLink = new Link(req.body);
  newLink.user = req.userId;

  // Save it into the DB.
  newLink.save((err,link) => {
    if(err) {
      res.send(err);
    }
    else { //If no errors, send it back to the client
      res.json({message: "Link successfully added!", link });
    }
  });

});


// POST request to upload Links from file 
router.post('/links/upload', upload.single('file'), VerifyToken, function (req, res) {

  if (req.file) {

    let data = req.file.buffer.toString();
    let uploadedLinks = JSON.parse(data);  

    let messages = [];  
    let warnings = [];  

    uploadedLinks.forEach(function(uploadedLink) {

      // Check if the uploaded link already exists
      Link.findById(uploadedLink._id, function (err, existingLink) {
        if (err) {
          console.log('err = ' + err); 
        }
        if (existingLink) {

          // TODO: update link according to update policy
          warnings.push({ message: 'IGNORED: A link with id ' + uploadedLink._id + ' already exists in this collection.' }); 

        } else {

          // Insert uploadedLink as new

          // TODO: insert link according owner policy
          // with existing userId - if user exists
          // without existing userId - with current user's id
          let link = new Link(uploadedLink); 

          link.save((err, obj) =>  {
            if (err) {
              console.log(err); 
            } else {
              messages.push('ADDED: Added link ' + link._id + ' as new.'); 
            }
          }); 
        }
      }); 

    }); 

    res.json({ message: "Links successfully uploaded." }); 

  } else {
    res.json({ error: "No file received." }); 
  }
 
}); 


// PUT request to update Link.
router.put('/links/:id', VerifyToken, function (req, res) {

  Link.findById({_id: req.params.id}, (err, link) => {

    if(err) res.send(err);

    // Update link only if it already exists in the db 
    if (link) {

      // Copy request parameter values to retrieved node
      Object.assign(link, req.body);
          
      // Update modifiedDate 
      link.modifiedDate = new Date(); 

      link.save((err, link) => {
        if(err) res.send(err);
        res.json({ message: 'Link updated!', link});
      });

    } else {
      res.redirect('/nodes/create');
    }
  }).populate(['user']);
});


// DELETE request to delete Link.
// router.delete('/links/:id', VerifyToken, function (req, res) {

//  Link.remove({_id : req.params.id}, (err, result) => {
//    res.json({ message: "Link successfully deleted!", result });
//  });

// });

// DELETE request to delete Link.
router.delete('/links/:id', VerifyToken, function (req, res) {

  Link.findById(req.params.id, function (err, link) {

    if(err) res.send(err);

    link.remove(function (err, link) {
      if(err) { 
        res.send(err); 
      } else {
        res.json({ message: 'Link successfully deleted!', link});
      }
    });
  }); 
}); 

// 
// NODE API 
//
// GET request for list of all Nodes.
router.get('/nodes', function (req, res) {

  let limit = 10;  
  let page = 1;
  let sorts = { modifiedDate: 'desc'}; 

  if (req.query.page) { 

    if (parseInt(req.query.page, 10)) {
      page = parseInt(req.query.page, 10);
      if (page < 0) { page = 1; }
    };

    // Query the DB and if no errors, send paginated nodes
    Node
      .paginate({}, { page: page, limit: limit, sort: sorts })
      .then((err, response) => {
        if(err) res.send(err);
        res.json(response);
      });

  } else {

    // Query the DB and if no errors, send all nodes
    Node
      .find({}, '')
      .sort(sorts)
      .then((err, response) => {
        if(err) res.send(err);
        res.json(response);
      });
  }

});

// POST request to upload Nodes from file 
router.post('/nodes/upload', upload.single('file'), VerifyToken, function (req, res) {

  if (req.file) {

    let data = req.file.buffer.toString();
    let uploadedNodes = JSON.parse(data);  

    let messages = [];  
    let warnings = [];  

    uploadedNodes.forEach(function(uploadedNode) {

      // Check if the uploaded node already exists
      Node.findById(uploadedNode._id, function (err, existingNode) {
        if (err) {
          console.log('err = ' + err); 
        }
        if (existingNode) {

          // TODO: update node according to update policy
          warnings.push({ message: 'IGNORED: A node with id ' + uploadedNode._id + ' already exists in this collection.' }); 

        } else {

          // Insert uploadedNode as new

          // TODO: insert node according owner policy
          // with existing userId - if user exists
          // without existing userId - with current user's id
          let node = new Node(uploadedNode); 

          node.save((err, obj) =>  {
            if (err) {
              console.log(err); 
            } else {
              messages.push('ADDED: Added node ' + node._id + ' as new.'); 
            }
          }); 
        }
      }); 

    });

    res.json({ message: "Nodes successfully uploaded." }); 

  } else {
    res.json({ message: "No file received." }); 
  }


});

// GET request for a specific Node.
router.get('/nodes/:id', function (req, res) {

  if (objectId.isValid(req.params.id)) {
    Node.findById(req.params.id)
    .populate(['user'])
    .then(function(err,node) {
      if (err) {
        res.send(err)
      }
      if (node === undefined) {
        try {
          res.status(404).send( {'error': 'The requested node does not exist.'} )
        } catch (e) {
        }
      } else {
        res.json(node)
      }
    });
  } else {
    res.status(404).send( {'error': 'The requested node does not exist.'} )
  }

});

// POST request to create new Node
router.post('/nodes', VerifyToken, function (req, res) {

  // Create a new node
  let newNode = new Node(req.body);
  newNode.user = req.userId;

  // Save it into the DB.
  newNode.save((err,node) => {
    if(err) {
      // Send server side errors 
      res.send(err);
    }
    else { 
      // If no errors, send it back to the client
      res.json({message: "Node successfully added!", node });
    }
  });

});

// PUT request to update Node.
router.put('/nodes/:id', VerifyToken, function (req, res) {

  Node.findById({_id: req.params.id}, (err, node) => {

    if(err) res.send(err);
    
    // Update node only if it already exists in the db 
    if (node) {

      // Copy request parameter values to retrieved node
      Object.assign(node, req.body);
    
      // Update modifiedDate 
      node.modifiedDate = new Date(); 

      node.save((err, node) => {
        if(err) res.send(err);
        res.json({ message: 'Node updated!', node });
      });

    } else {
      res.redirect('/nodes/create'); 
    }
  }).populate(['user']);
}); 


// DELETE request to delete Node.
router.delete('/nodes/:id', VerifyToken, function (req, res) {

  Node.findById(req.params.id, function (err, node) {

    if(err) res.send(err);

    node.remove(function (err, node) {
      if(err) { 
        res.send(err); 
      } else {
        res.json({ message: 'Node successfully deleted!', node });
      }
    });
  }); 
}); 

// 
// PROJECT API 
//
// GET request for list of all Projects.
router.get('/projects', function (req, res) {

  let limit = 10;  
  let page = 1; 
  let sorts = { modifiedDate: 'desc'}; 

  if (req.query.page) { 

    if (parseInt(req.query.page, 10)) {
      page = parseInt(req.query.page, 10);
      if (page < 0) { page = 1; }
    };

    // Query the DB and if no errors, send paginated projects
    Project
      .paginate({}, { page: page, limit: limit, sort: sorts })
      .then((err, response) => {
        if(err) res.send(err);
        res.json(response);
      });

  } else {

    // Query the DB and if no errors, send all projects
    Project
      .find({}, '')
      .sort(sorts)
      .then((err, response) => {
        if(err) res.send(err);
        res.json(response);
      });
  }

});


// GET request for a specific Project.
router.get('/projects/:id', function (req, res) {

  if (objectId.isValid(req.params.id)) {
    Project.findById(req.params.id)
    .populate(['user'])
    .then(function(err,project) {
      if (err) {
        res.send(err)
      }
      if (project === undefined) {
        try {
          res.status(404).send( {'error': 'The requested project does not exist.'} )
        } catch (e) {
        }
      } else {
        res.json(project)
      }
    });
  } else {
    res.status(404).send( {'error': 'The requested project does not exist.'} )
  }

});

// POST request to create new Project
router.post('/projects', VerifyToken, function (req, res) {

  // Create a new project
  let newProject = new Project(req.body);
  newProject.user = req.userId;

  // Save it into the DB.
  newProject.save((err,project) => {
    if(err) {
      // Send server side errors 
      res.send(err);
    }
    else { 
      // If no errors, send it back to the client
      res.json({message: "Project successfully added!", project });
    }
  });

});


// POST request to upload Projects from file 
router.post('/projects/upload', upload.single('file'), VerifyToken, function (req, res) {

  if (req.file) {

    let data = req.file.buffer.toString();
    let uploadedProjects = JSON.parse(data);  

    let messages = [];  
    let warnings = [];  

    uploadedProjects.forEach(function(uploadedProject) {

      // Check if the uploaded project already exists
      Project.findById(uploadedProject._id, function (err, existingProject) {
        if (err) {
          console.log('err = ' + err); 
        }
        if (existingProject) {

          // TODO: update project according to update policy
          warnings.push({ message: 'IGNORED: A project with id ' + uploadedProject._id + ' already exists in this collection.' }); 

        } else {

          // Insert uploadedProject as new

          // TODO: insert project according owner policy
          // with existing userId - if user exists
          // without existing userId - with current user's id
          let project = new Project(uploadedProject); 

          project.save((err, obj) =>  {
            if (err) {
              console.log(err); 
            } else {
              messages.push('ADDED: Added project ' + project._id + ' as new.'); 
            }
          }); 
        }
      }); 

    }); 

    res.json({ message: "Projects successfully uploaded." }); 

  } else {
    res.json({ message: "No file received." }); 
  }
 
}); 


// PUT request to update Project.
router.put('/projects/:id', VerifyToken, function (req, res) {

  Project.findById({_id: req.params.id}, (err, project) => {

    if(err) res.send(err);
    
    // Update project only if it already exists in the db 
    if (project) {
      
      // Copy request parameter values to retrieved project
      Object.assign(project, req.body);

      // Update modifiedDate 
      project.modifiedDate = new Date(); 

      project.save((err, project) => {
        if(err) res.send(err);
        res.json({ message: 'Project updated!', project });
      }); 
    } else {
      res.redirect('/projects/create'); 
    }
  }).populate(['user']);
}); 

// DELETE request to delete Project.
router.delete('/projects/:id', VerifyToken, function (req, res) {

  Project.findById(req.params.id, function (err, project) {

    if(err) res.send(err);

    project.remove(function (err, project) {
      if(err) { 
        res.send(err); 
      } else {
        res.json({ message: 'Project successfully deleted!', project });
      }
    });
  }); 
}); 

// 
// USER API 
//
// GET request for list of all Users.
router.get('/users', function (req, res) {

  let limit = 10;  
  let page = 1; 
  let sorts = { modifiedDate: 'desc'}; 

  if (req.query.page) { 

    if (parseInt(req.query.page, 10)) {
      page = parseInt(req.query.page, 10);
      if (page < 0) { page = 1; }
    };

    // Query the DB and if no errors, send paginated users
    User
      .paginate({}, { page: page, limit: limit, sort: sorts })
      .then((err, response) => {
        if(err) res.send(err);
        res.json(response);
      });

  } else {

    // Query the DB and if no errors, send all users
    User
      .find({}, '')
      .sort(sorts)
      .then((err, response) => {
        if(err) res.send(err);
        res.json(response);
      });
  }

});


// GET request for a specific User.
router.get('/users/:id', function (req, res) {

  if (objectId.isValid(req.params.id)) {
    User.findById(req.params.id).then(function(err,user) {
      if (err) {
        res.send(err)
      }
      if (user === undefined) {
        try {
          res.status(404).send( {'error': 'The requested user does not exist.'} )
        } catch (e) {
        }
      } else {
        res.json(user)
      }
    });
  } else {
    res.status(404).send( {'error': 'The requested user does not exist.'} )
  }

});


// GET request for a user's number of links.
router.get('/users/:id/links', function (req, res) {

  if (objectId.isValid(req.params.id)) {
  
    let limit = 1; // because were only interested in the total number  
    let page = 1; 
    let sorts = { modifiedDate: 'desc'};

    Link 
      .paginate({ user: req.params.id }, { page: page, limit: limit, sort: sorts })
      .then((err, response) => {
        if(err) { res.send(err); }
        res.json(response);
      });

  } else {
    res.status(404).send( {'error': 'The requested user does not exist.'} )
  }

});

// GET request for a user's number of nodes.
router.get('/users/:id/nodes', function (req, res) {

  if (objectId.isValid(req.params.id)) {

    let limit = 1; // because we are only interested in the total number 
    let page = 1; 
    let sorts = { modifiedDate: 'desc'};

    Node 
      .paginate({ user: req.params.id }, { page: page, limit: limit, sort: sorts })
      .then((err, response) => {
        if(err) { res.send(err); }
        res.json(response);
      });

  } else {
    res.status(404).send( {'error': 'The requested user does not exist.'} )
  }

});

// GET request for a user's number of projects.
router.get('/users/:id/projects', function (req, res) {

  if (objectId.isValid(req.params.id)) {

    let limit = 1; // because we are only interested in the total number
    let page = 1; 
    let sorts = { modifiedDate: 'desc'};

    Project 
      .paginate({ user: req.params.id }, { page: page, limit: limit, sort: sorts })
      .then((err, response) => {
        if(err) { res.send(err); }
        res.json(response);
      });

  } else {
    res.status(404).send( {'error': 'The requested user does not exist.'} )
  }

});

// POST request to create new User
router.post('/users', VerifyToken, function (req, res) {

  // Create a new user
  let newUser = new User(req.body);
  newUser.user = req.userId;

  // Save it into the DB.
  newUser.save((err,user) => {
    if(err) {
      // Send server side errors 
      res.send(err);
    }
    else { 
      // If no errors, send it back to the client
      res.json({message: "User successfully added!", user });
    }
  });

});


// POST request to upload Users from file 
router.post('/users/upload', upload.single('file'), VerifyToken, function (req, res) {

  if (req.file) {

    let data = req.file.buffer.toString();
    let uploadedUsers = JSON.parse(data);  

    let messages = [];  
    let warnings = [];  

    uploadedUsers.forEach(function(uploadedUser) {

      // Check if the uploaded user already exists
      User.findById(uploadedUser._id, function (err, existingUser) {
        if (err) {
          console.log('err = ' + err); 
        }
        if (existingUser) {

          // TODO: update user according to update policy
          warnings.push({ message: 'IGNORED: A user with id ' + uploadedUser._id + ' already exists in this collection.' }); 

        } else {

          // Insert uploadedUser as new

          // TODO: insert user according owner policy
          // with existing userId - if user exists
          // without existing userId - with current user's id

          User.register(uploadedUser, uploadedUser.password, function(err) {
            if (err) {
              console.log(err); 
            } else {
              messages.push('ADDED: Added user ' + uploadedUser._id + ' as new.'); 
            }
          }); 
        }
      }); 

    }); 

    res.json({ message: "Users successfully uploaded." }); 

  } else {
    res.json({ message: "No file received." }); 
  }
 
}); 


// PUT request to update User.
router.put('/users/:id', VerifyToken, function (req, res) {

  User.findById({_id: req.params.id}, (err, user) => {

    if(err) res.send(err);

    // Update user only if it already exists in the db 
    if (user) {

      // Copy request parameter values to retrieved user
      Object.assign(user, req.body);
    
      // Restore original id
      user._id = req.params.id; 
  
      user.save((err, user) => {
        if(err) res.send(err);
        res.json({ message: 'User updated!', user });
      }); 
    } else {
      res.redirect('/users/create'); 
    }
  });
}); 


// DELETE request to delete User.
router.delete('/users/:id', VerifyToken, function (req, res) {

  User.findById(req.params.id, function (err, user) {

    if(err) res.send(err);

    user.remove(function (err, user) {
      if(err) { 
        res.send(err); 
      } else {
        res.json({ message: 'User successfully deleted!', user });
      }
    });
  }); 
}); 

module.exports = router;

