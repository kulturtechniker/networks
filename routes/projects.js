const express = require('express');
const multer = require('multer');
const router = express.Router();

// Require controller modules.
const projectController = require('../controllers/projectController');
const Authenticate = require('../middleware/Authenticate');
const GetUser = require('../middleware/GetUser');

// Store uploaded files in memory
const storage = multer.memoryStorage();
const upload = multer({storage: storage});


/// NODE ROUTES ///

// GET request for list of all Projects.
router.get('/', GetUser, projectController.project_list);

// GET request for creating Project.
router.get('/create', GetUser, Authenticate, projectController.project_create_get);

// POST request for creating Project.
router.post('/create', GetUser, Authenticate, projectController.project_create_post);

// GET request for uploading Links.
router.get('/upload', GetUser, Authenticate, projectController.uploadProjectsGet);

// POST request for uploading Links.
router.post('/upload', upload.single('file'), GetUser, Authenticate, projectController.uploadProjectsPost);

// GET request for one Project.
router.get('/:id', GetUser, projectController.project_detail); 

// POST request to update Project.
router.post('/:id', GetUser, Authenticate, projectController.project_update_post);

// GET request to delete Project.
router.get('/:id/delete', GetUser, Authenticate, projectController.project_delete_get);

// POST request to delete Project.
router.post('/:id/delete', GetUser, Authenticate, projectController.project_delete_post);

module.exports = router;

