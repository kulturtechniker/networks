const express = require('express');
const multer = require('multer');
const router = express.Router();

// Require controller modules.
const link_controller = require('../controllers/linkController');
const Authenticate = require('../middleware/Authenticate');
const GetUser = require('../middleware/GetUser');

// Store uploaded files in memory
const storage = multer.memoryStorage();
const upload = multer({storage: storage});


/// LINK ROUTES ///

// GET request for list of all Links.
router.get('/', GetUser, link_controller.link_list);

// GET request for creating Link.
router.get('/create', GetUser, Authenticate, link_controller.link_create_get);

// POST request for creating Link.
router.post('/create', GetUser, Authenticate, link_controller.link_create_post);

// GET request for uploading Links.
router.get('/upload', GetUser, Authenticate, link_controller.uploadLinksGet);

// POST request for uploading Links.
router.post('/upload', upload.single('file'), GetUser, Authenticate, link_controller.uploadLinksPost);

// GET request for one Link.
router.get('/:id', GetUser, link_controller.link_detail);

// POST request to update Link.
router.post('/:id', GetUser, Authenticate, link_controller.link_update_post);

// GET request to delete Link.
router.get('/:id/delete', GetUser, Authenticate, link_controller.link_delete_get);

// POST request to delete Link.
router.post('/:id/delete', GetUser, Authenticate, link_controller.link_delete_post);

module.exports = router;

