const express = require('express');
const router = express.Router();

// Require controller modules.
const auth_controller = require("../controllers/authController"); 
const GetUser = require('../middleware/GetUser');

router.get('/', GetUser, auth_controller.index);

// route to account page
router.get('/account', GetUser, auth_controller.account_get);

// route to chart page
router.get('/chart', GetUser, function(req, res, next) {
  res.render('chart', { title: 'Network Chart', section: '/chart', user: req.user });
});

// route to help page
router.get('/help', GetUser, function(req, res, next) {
  res.render('help', { title: 'Help', section: '/help', user: req.user });
});

// route to sign-up page
router.get('/sign-up', GetUser, auth_controller.register_get);

// route for sign-up action
router.post('/sign-up', GetUser, auth_controller.register_post);

// route to sign-in page
router.get('/sign-in', GetUser, auth_controller.login_get);

// route for sign-in action
router.post('/sign-in', GetUser, auth_controller.login_post);

// route for sign-out action
router.get('/sign-out', auth_controller.logout);

module.exports = router;

