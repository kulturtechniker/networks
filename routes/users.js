const express = require('express');
const multer = require('multer');
const router = express.Router();

// Require controller modules.
const userController = require('../controllers/userController');
const Authenticate = require('../middleware/Authenticate');
const GetUser = require('../middleware/GetUser');

// Store uploaded files in memory
const storage = multer.memoryStorage();
const upload = multer({storage: storage});


/// USER ROUTES ///

// GET request for list of all Users.
router.get('/', GetUser, userController.getUsers);

// GET request for creating User.
//router.get('/create', GetUser, Authenticate, userController.createUserGet);

// POST request for creating User.
//router.post('/create', GetUser, Authenticate, userController.createUserPost);

// GET request for uploading Users 
router.get('/upload', GetUser, Authenticate, userController.uploadUsersGet);

// POST request for uploading Users.
router.post('/upload', upload.single('file'), GetUser, Authenticate, userController.uploadUsersPost);

// GET request for one User.
router.get('/:id', GetUser, userController.getUser); 

// POST request to update User.
//router.post('/:id', GetUser, Authenticate, userController.updateUser);

// GET request to delete User.
// router.get('/:id/delete', GetUser, Authenticate, userController.deleteUserGet);

// POST request to delete User.
//router.post('/:id/delete', GetUser, Authenticate, userController.deleteUserPost);

module.exports = router;

