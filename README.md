# Objectives 

* Collect link data in a format like https://bl.ocks.org/mbostock/4062045#miserables.json
* Display data with https://bl.ocks.org/mbostock/4062045

# Prerequisites

Networks is developed using npm 6.1.0

For information on how to install a current node version on Debian Linux see: 
https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions

# How to build

TODO

# How to run

Change working directory to networks

```cd networks```

Launch development server with

```DEBUG=networks:* npm run devstart```


# How to test

Change working directory to networks

```cd networks```

Run

```npm test```
   

# How to contribute

TODO