#! /usr/bin/env node

console.log('This script populates some test nodes, links, and users to your database. Specified database as argument - e.g.: populatedb mongodb://your_username:your_password@your_dabase_url');

// Get arguments passed on command line
const userArgs = process.argv.slice(2);
if (!userArgs[0].startsWith('mongodb://')) {
    console.log('ERROR: You need to specify a valid mongodb URL as the first argument');
    return
}

const async = require('async');
const Link = require('./models/link');
const Node = require('./models/node');
const Project = require('./models/project');
const User = require('./models/user');

const mongoose = require('mongoose');
const mongoDB = userArgs[0];
mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
const db = mongoose.connection;
mongoose.connection.on('error', console.error.bind(console, 'MongoDB connection error:'));

const links = [];
const nodes = [];
const projects = [];
const users = [];

function linkCreate(user, source, target, value, cb) {

  let linkdetail = {
    createDate: new Date(),
    modifiedDate: new Date(),
    user: user,
    source:source, 
    target:target, 
    value:value 
  }
  
  let link = new Link(linkdetail);
       
  link.save(function (err) {
    if (err) {
      cb(err, null)
      return
    }
    console.log('New Link: ' + link);
    links.push(link)
    cb(null, link)
  });
}

function nodeCreate(user, project, id, group, cb) {

  let nodedetail = {
    createDate: new Date(),
    modifiedDate: new Date(),
    project: project,
    user: user,
    id:id, 
    group:group
  }
  
  let node = new Node(nodedetail);
       
  node.save(function (err) {
    if (err) {
      cb(err, null)
      return
    }
    console.log('New Node: ' + node);
    nodes.push(node)
    cb(null, node)
  });
}

function projectCreate(user, title, description, cb) {

  let projectdetail = {
    user: user,
    title:title, 
    description:description 
  }
  
  let project = new Project(projectdetail);
       
  project.save(function (err) {
    if (err) {
      cb(err, null)
      return
    }
    console.log('New Project: ' + project);
    projects.push(project)
    cb(null, project)
  });
}

function userCreate(first_name, last_name, email, cb) {

  let userdetail = {
    createDate: new Date(),
    modifiedDate: new Date(),

    email:email,
    first_name: first_name,
    last_name: last_name,
    password: 'test'
  }

  let user = new User(userdetail);

  User.register(user, 'test', function (err) {
    if (err) {
      cb(err, null)
      return
    }
    users.push(user)
    cb(null, user)
  });
}

function createLinks(cb) {
  async.parallel([
    function(callback) {
      linkCreate(users[0], nodes[1], nodes[0], 1, callback)
    },
    function(callback) {
      linkCreate(users[1], nodes[2], nodes[0], 8, callback)
    },
    function(callback) {
      linkCreate(users[0], nodes[3], nodes[0], 10, callback)
    },
    function(callback) {
      linkCreate(users[0], nodes[3], nodes[2], 6, callback)
    },
    function(callback) {
      linkCreate(users[0], nodes[4], nodes[0], 1, callback)
    },
    function(callback) {
      linkCreate(users[1], nodes[5], nodes[0], 1, callback)
    },
    function(callback) {
      linkCreate(users[0], nodes[6], nodes[0], 1, callback)
    },
    function(callback) {
      linkCreate(users[1], nodes[7], nodes[0], 1, callback)
    },
    function(callback) {
      linkCreate(users[1], nodes[9], nodes[0], 2, callback)
    },
    function(callback) {
      linkCreate(users[1], nodes[11], nodes[11], 1, callback)
    },
    function(callback) {
      linkCreate(users[1], nodes[11], nodes[3], 3, callback)
    },
    function(callback) {
      linkCreate(users[1], nodes[11], nodes[2], 3, callback)
    },
    function(callback) {
      linkCreate(users[1], nodes[11], nodes[0], 5, callback)
    },
    function(callback) {
      linkCreate(users[1], nodes[12], nodes[11], 1, callback)
    },
    function(callback) {
      linkCreate(users[1], nodes[13], nodes[11], 1, callback)
    },
    function(callback) {
      linkCreate(users[1], nodes[14], nodes[11], 1, callback)
    },
    function(callback) {
      linkCreate(users[1], nodes[15], nodes[11], 1, callback)
    },
    function(callback) {
      linkCreate(users[1], nodes[16], nodes[17], 4, callback)
    },
    function(callback) {
      linkCreate(users[1], nodes[18], nodes[17], 4, callback)
    },
    function(callback) {
      linkCreate(users[1], nodes[18], nodes[16], 4, callback)
    }
  ],
  // Optional callback
  cb);
}

function createNodes(cb) {
  async.parallel([
    function(callback) {
      // node[0]
      nodeCreate(users[0], projects[0], 'Myriel', 1, callback)
    },
    function(callback) {
      // node[1]
      nodeCreate(users[0], projects[0], 'Napoleon', 1, callback)
    },
    function(callback) {
      // node[2]
      nodeCreate(users[1], projects[0], 'Mlle.Baptistine', 1, callback)
    },
    function(callback) {
      // node[3]
      nodeCreate(users[0], projects[0], 'Mme.Magloire', 1, callback)
    },
    function(callback) {
      // node[4]
      nodeCreate(users[0], projects[0], 'CountessdeLo', 1, callback)
    },
    function(callback) {
      // node[5]
      nodeCreate(users[0], projects[0], 'Geborand', 1, callback)
    },
    function(callback) {
      // node[6]
      nodeCreate(users[0], projects[0], 'Champtercier', 1, callback)
    },
    function(callback) {
      // node[7]
      nodeCreate(users[0], projects[0], 'Cravatte', 1, callback)
    },
    function(callback) {
      // node[8]
      nodeCreate(users[0], projects[0], 'Count', 1, callback)
    },
    function(callback) {
      // node[9]
      nodeCreate(users[0], projects[0], 'OldMan', 1, callback)
    },
    function(callback) {
      // node[10]
      nodeCreate(users[1], projects[0], 'Labarre', 2, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Valjean', 2, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Marguerite', 3, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Mme.deR', 2, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Isabeau', 2, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Gervais', 2, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Tholomyes', 3, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Listolier', 3, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Fameuil', 3, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Blacheville', 3, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Favourite', 3, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Dahlia', 3, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Zephine', 3, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Fantine', 3, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Mme.Thenardier', 4, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Thenardier', 4, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Fauchelevent', 0, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Bamatabois', 2, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Perpetue', 3, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Simplifice', 2, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Scaufflaire', 2, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Woman1', 2, callback)
    },
    function(callback) {
      nodeCreate(users[1], projects[0], 'Judge', 2, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Champmathieu', 2, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Brevet', 2, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Chenildieu', 2, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Cochepaille', 2, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Pontmercy', 4, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Boulatruelle', 6, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Eponine', 4, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Anzelma', 4, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Woman2', 5, callback)
    },
    function(callback) {
      nodeCreate(users[1], projects[0], 'MotherInnocent', 0, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Gribier', 0, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Jondrette', 7, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Mme.Burgon', 7, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Gavroche', 8, callback)
    },
    function(callback) {
      nodeCreate(users[1], projects[0], 'Gillenormand', 5, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Magnon', 5, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Mlle.Gillenormand', 5, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Mme.Pontmercy', 5, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Mlle.Vaubois', 5, callback)
    },
    function(callback) {
      nodeCreate(users[1], projects[0], 'Lt.Gillenormand', 5, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Marius', 8, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'BaronessT', 5, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Mabeuf', 8, callback)
    },
    function(callback) {
      nodeCreate(users[1], projects[0], 'Enjolras', 8, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Combeferre', 8, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Prouvaire', 8, callback)
    },
    function(callback) {
      nodeCreate(users[1], projects[0], 'Feuilly', 8, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Courfeyrac', 8, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Bahorel', 8, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Bossuet', 8, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Joly', 8, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Grantaire', 8, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'MotherPlutarch', 9, callback)
    },
    function(callback) {
      nodeCreate(users[1], projects[0], 'Gueulemer', 4, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Babet', 4, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Claquesous', 4, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Montparnasse', 4, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Toussaint', 5, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Child1', 10, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Child2', 10, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Brujon', 4, callback)
    },
    function(callback) {
      nodeCreate(users[0], projects[0], 'Mme.Hucheloup', 8, callback)
    }
  ],
  //Optional callback
  cb);
}

function createProjects(cb) {
  async.parallel([
    function(callback) {
      projectCreate(users[0], 'Les Misérables', 'The social networks in Victor Hugo\'s novel Les Misérables', callback);
    },
    function(callback) {
      projectCreate(users[1], 'Le Vite di Vasari', 'The genealogy of italian art according to Vasari', callback);
    }
  ],
  // optional callback
  cb);
}

function createUsers(cb) {
  async.parallel([
    function(callback) {
      userCreate('Bob', 'Peterson', 'bob.peterson@example.com', callback);
    },
    function(callback) {
      userCreate('Alice', 'Myer', 'alice.myer@example.com', callback);
    }
  ],
  // optional callback
  cb);
}

function removeLinks(cb) {
  Link.find({}).remove(cb);
}

function removeNodes(cb) {
  Node.find({}).remove(cb);
}

function removeProjects(cb) {
  Project.find({}).remove(cb);
}

function removeUsers(cb) {
  User.find({}).remove(cb);
}

async.series([
    
    // Remove existing documents
    removeLinks,
    removeNodes,
    removeProjects,
    removeUsers,

    createUsers,
    createProjects,
    createNodes,
    createLinks
],
// Optional callback
function(err, results) {
    if (err) {
        console.log('FINAL ERR: '+err);
    }
    // All done, disconnect from database
    mongoose.connection.close();
});

