const moment = require('moment');
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const Schema = mongoose.Schema;

const LinkSchema = new Schema(
  {
    createDate: {type: Date, default: Date.now },
    modifiedDate: {type: Date, default: Date.now },
    user: { type: Schema.ObjectId, ref: 'User', required: true },

    source: {type: Schema.ObjectId, ref: 'Node', required: true},
    target: {type: Schema.ObjectId, ref: 'Node', required: true},
    value: {type: Number, default: 1},
  }
);

// Export virtuals to JSON
LinkSchema.set('toJSON', { virtuals: true })


// Virtual for a link's createDate 
LinkSchema
  .virtual('createDateFormatted')
  .get(function () {
    return moment(this.createDate).format('MMMM Do, YYYY HH:mm');
});

// Virtual for a link's modifiedDate 
LinkSchema
  .virtual('modifiedDateFormatted')
  .get(function () {
    return moment(this.modifiedDate).format('MMMM Do, YYYY HH:mm');
});

// Virtual for a link's URL
LinkSchema
  .virtual('url')
  .get(function () {
    return '/links/' + this._id;
});


// Plugin pagination 
LinkSchema.plugin(mongoosePaginate);

//Export model
module.exports = mongoose.model('Link', LinkSchema);

