const moment = require('moment');
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const Link = require('./link');
const Schema = mongoose.Schema;

const NodeSchema = new Schema(
  {
    createDate: {type: Date, default: Date.now },
    modifiedDate: {type: Date, default: Date.now },
    project: { type: Schema.ObjectId, ref: 'Project', required: true },
    user: { type: Schema.ObjectId, ref: 'User', required: true },

    id: {type: String, required: true, max: 100},
    group: {type: Number},
  }
);

// Check for dependent links
NodeSchema.pre('remove', function(next) {

  // Link might have this node either as source or as target
  let query = { $or: [ { source: this._id }, { target: this._id } ] };

  Link.countDocuments(query, function(err, count){
    if (count > 0) {

      // Preliminary implementation after the model of ValidationError
      let error = {'errors' : { message: 'Delete node failed. ' + count + ' links depend on this node.' } }; 
      next(error); 

    } else {
      next();
    }
  });  
});


// Export virtuals to JSON
NodeSchema.set('toJSON', { virtuals: true })


// Virtual for a node's createDate 
NodeSchema
  .virtual('createDateFormatted')
  .get(function () {
    return moment(this.createDate).format('MMMM Do, YYYY HH:mm');
});

// Virtual for a node's modifiedDate 
NodeSchema
  .virtual('modifiedDateFormatted')
  .get(function () {
    return moment(this.modifiedDate).format('MMMM Do, YYYY HH:mm');
});

// Virtual for a node's URL
NodeSchema
  .virtual('url')
  .get(function () {
    return '/nodes/' + this._id;
});


// Plugin pagination 
NodeSchema.plugin(mongoosePaginate);

// Export model
module.exports = mongoose.model('Node', NodeSchema);

