const moment = require('moment');
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const Node = require('./node');
const Schema = mongoose.Schema;

// Model fields
const projectSchema = new Schema({

  createDate: { type: Date, default: Date.now },
  modifiedDate: { type: Date, default: Date.now },
  user: { type: Schema.ObjectId, ref: 'User', required: true },

  title: { type: String, required: true }, 
  description: { type: String } 

}); 


// Check for dependent nodes 
projectSchema.pre('remove', function(next) {

  let query = {  project: this._id };
    Node.countDocuments(query, function(err, count){
      if (count > 0) {
        // Preliminary implementation after the model of ValidationError
        let error = {'errors' : { message: 'Delete node failed. ' + count + ' links depend on this node.' } };
        next(error);
      } else {
        next();
      }
  });
});


// Export virtuals to JSON
projectSchema.set('toJSON', { virtuals: true })


// Virtual for a project's createDate 
projectSchema
  .virtual('createDateFormatted')
  .get(function () {
    return moment(this.createDate).format('MMMM Do, YYYY HH:mm');
});

// Virtual for a project's modifiedDate 
projectSchema
  .virtual('modifiedDateFormatted')
  .get(function () {
    return moment(this.modifiedDate).format('MMMM Do, YYYY HH:mm');
});

// Virtual for a project's URL
projectSchema
  .virtual('url')
  .get(function () {
    return '/projects/' + this._id;
});


// Plugin pagination 
projectSchema.plugin(mongoosePaginate);

// Export model
module.exports = mongoose.model('Project', projectSchema);

