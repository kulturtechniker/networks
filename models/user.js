const moment = require('moment');
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const passportLocalMongoose = require('passport-local-mongoose');

const Link = require('./link');
const Node = require('./node');

const Schema = mongoose.Schema;

const UserSchema = new Schema(
  {
    createDate: { type: Date, default: Date.now },
    modifiedDate: { type: Date, default: Date.now },

    first_name: { type: String, max: 100 },
    last_name: { type: String, max: 100 },
    email: { type: String, unique: true, required: true, trim: true }
  }
);

// Plugin passport-local for mongoose 
UserSchema.plugin(passportLocalMongoose, { usernameField: 'email' });

// Check for dependent links
UserSchema.pre('remove', function(next) {

  let query = { user: this._id };

  Link.count(query, function(err, count) {

    if (count > 0) {

      // Preliminary implementation after the model of ValidationError
      let error = {'errors' : { message: 'Delete user failed. ' + count + ' links depend on this user.' } };
      next(error);

    } else {
      next();
    }
  });

  Node.count(query, function(err, count) {

    if (count > 0) {

      // Preliminary implementation after the model of ValidationError
      let error = {'errors' : { message: 'Delete user failed. ' + count + ' nodes depend on this user.' } };
      next(error);

    } else {
      next();
    }
  });

});


// Export virtuals to JSON
UserSchema.set('toJSON', { virtuals: true })

// Virtual for user's fullName 
UserSchema
  .virtual('full_name')
  .get(function () {
    return this.first_name + ' ' + this.last_name;
  });

// Virtual for a user's createDate 
UserSchema
  .virtual('createDateFormatted')
  .get(function () {
    return moment(this.createDate).format('MMMM Do, YYYY HH:mm');
  });

// Virtual for a user's modifiedDate 
UserSchema
  .virtual('modifiedDateFormatted')
  .get(function () {
    return moment(this.modifiedDate).format('MMMM Do, YYYY HH:mm');
  });

// Virtual for user's URL
UserSchema
  .virtual('url')
  .get(function () {
    return '/users/' + this._id;
  });

// Plugin pagination 
UserSchema.plugin(mongoosePaginate);

//Export model
module.exports = mongoose.model('User', UserSchema)

