const { body,validationResult } = require('express-validator/check');
const config = require('config');
const fetch = require('node-fetch');
const FormData = require('form-data');
const User = require('../models/user');
const { sanitizeBody } = require('express-validator/filter');

const apiUrl = config.API_URL;


// Display paginated list of Users.
exports.getUsers = async (req, res, next) => {

  let page = 1; 
  if (parseInt(req.query.page, 10)) {
    page = parseInt(req.query.page, 10); 
  };

  try {

    let data = await fetch(apiUrl + '/users' + '?page=' + page)
      .then(res => res.json())

    res.render('users', { title: 'Network Users', data: data, section: '/users', user_: req.user });

  } catch (e) {
    console.log(e); 
    res.render('error', { message: 'An unexpected error ocurred.', section: '/users', user_: req.user });
  }
};


// Display detail page for a specific User.
exports.getUser = function (req, res, next) {

  Promise.all([
    fetch(apiUrl + '/users/' + req.params.id).then(res => res.json()),  
    fetch(apiUrl + '/users/' + req.params.id + '/links').then(res => res.json()), 
    fetch(apiUrl + '/users/' + req.params.id + '/nodes').then(res => res.json()), 
    fetch(apiUrl + '/users/' + req.params.id + '/projects').then(res => res.json())
  ]).then(([user, links, nodes, projects]) =>
      res.render('user', { links: links, nodes: nodes, projects: projects, section: '/users', user: user })
    ).catch(err => 
      res.render('error', { message: 'An unexpected error ocurred.', section: '/users', user_: req.user })
    )

};

// Display User create form on GET.
exports.userCreateGet = function(req, res, next) {
  res.render('user', { title: 'New User', section: '/users', user: req.user } );
};

// Handle User create on POST.
exports.userCreatePost =  [

  // TODO: Validate fields.

  // Sanitize fields.
  sanitizeBody('title').trim().escape(),
  sanitizeBody('description').trim().escape(),

  // Process request after validation and sanitization.
  async (req, res, next) => {

    try {

      let user = { title: req.body.title, description: req.body.description };

      let data = await fetch(apiUrl + '/users/', { 
        method: 'POST', 
        headers: {
          "Content-Type": "application/json",
          "x-access-token": req.cookies.token 
        },
        body: JSON.stringify(user) }
      )
        .then(res => res.json())

      // Get server-side validation errors from the REST request.
      let errors = data.errors;

      if (errors) {
        
        // There are errors. Render form again with sanitized values/error messages.
        res.render('user', { title: 'Error', errors: errors, section: '/users', user: req.user });

      } else {
        res.render('user', { title: 'User', message: data.message, user: data.user, section: '/users', user: req.user } );
      }

    } catch (e) {
      res.render('error', { message: 'An unexpected error ocurred.', section: '/users', user: req.user });
    }
  }
];

// Handle User update on POST.
exports.updateUser = [

  // TODO: Validate fields.

  // Sanitize fields.
  sanitizeBody('title').trim().escape(),
  sanitizeBody('description').trim().escape(),

  // Process request after validation and sanitization.
  async (req, res, next) => {

    try {
  
      let user = { title: req.body.title, description: req.body.description };

      let data = await fetch(apiUrl + '/users/' + req.params.id, { 
        method: 'PUT', 
        headers: {
          "Content-Type": "application/json",
          "x-access-token": req.cookies.token 
        },
        body: JSON.stringify(user) }
      )
        .then(res => res.json())

      // Get server-side validation errors from the REST request.
      let errors = data.errors;

      if (errors) {

        // There are errors. Render form again with sanitized values/error messages.
        res.render('user', { title: 'Error', user: user, errors: errors, section: '/users', user: req.user });

      } else {
        res.render('user', { title: 'Edit User', message: data.message, user: data.user, section: '/users', user: req.user } );
      }

    } catch (e) {
      res.render('error', { message: 'An unexpected error ocurred.', section: '/users', user: req.user });
    }
  }
];

// Display User delete form on GET.
exports.deleteUserGet = async (req, res, next) => {

  try {

    let data = {}; 

    data.user = await fetch(apiUrl + '/users/'  + req.params.id)
      .then(res => res.json())

//    data.nodes = await fetch(apiUrl + '/nodes?populate=true&user=' + req.params.id)
//      .then(res => res.json())

    // TODO: Handle errors
  
    // Successful, so render.
    res.render('user_delete', { title: 'Delete User',  user: data.user, section: '/users', user: req.user } );
//    res.render('user_delete', { title: 'Delete User', nodes: data.nodes, user: data.user, section: '/users', user: req.user } );

  } catch (e) {
    res.render('error', { title: 'Error', message: 'An unexpected error ocurred.', section: '/users', user: req.user });
  }

};

// Handle User delete on POST.
exports.deleteUserGet = async (req, res, next) => {

  try {
  
    let data = {}; 

    data = await fetch(apiUrl + '/users/' + req.params.id, { 
      method: 'DELETE', 
      headers: { 
        'Content-Type': 'application/json',
        'x-access-token': req.cookies.token
      }})
      .then(res => res.json())

    let errors = data.errors;

    if (errors) {
        
      // There are errors. Render the error page with error messages.
      res.render('error', { title: 'Error',  message: errors.message, section: '/users', user: req.user } );

    } else {

      // Success - render success page 
      res.render('success', { message: data.message, section: '/users', user: req.user } );
    }

  } catch (e) {
    res.render('error', { message: 'An unexpected error ocurred.', section: '/users', user: req.user });
  }

};

// Display Users upload form on GET.
exports.uploadUsersGet = function (req, res, next) {
  res.render('users_upload', { title: 'Upload Users', section: '/users', user: req.user } );
};

// Handle Users upload on POST.
exports.uploadUsersPost = async (req, res, next) => {

  try {

    let formData = new FormData();
    formData.append('file', req.file.buffer, req.file.originalname );

    let data = await fetch(apiUrl + '/users/upload', {
      method: 'POST',
      headers: {
        'x-access-token': req.cookies.token
      },
      body: formData }
    )

    let response = await data.json();
    res.render('users_upload', { title: 'Upload Users', message: response.message, section: '/users', user: req.user } );

  } catch (e) {
    console.log(e);
    res.render('error', { message: 'An unexpected error ocurred.', section: '/users', user: req.user });
  }

};

