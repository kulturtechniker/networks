const { body,validationResult } = require('express-validator/check');
const config = require('config');
const { sanitizeBody } = require('express-validator/filter');
const fetch = require("node-fetch");
const FormData = require('form-data');
const Node = require('../models/node');

const apiUrl = config.API_URL;


// Display paginated list of Nodes.
exports.node_list = async (req, res, next) => {

  let page = 1; 
  if (parseInt(req.query.page, 10)) {
    page = parseInt(req.query.page, 10); 
  };

  try {

    let data = await fetch(apiUrl + '/nodes/' + '?page=' + page)
      .then(res => res.json())

    res.render('nodes', { title: 'Network Nodes', data: data, section: '/nodes', user: req.user });

  } catch (e) {
    console.log(e);
    res.render('error', { message: 'An unexpected error ocurred.', section: '/nodes', user: req.user });
  }
};

// Display detail page for a specific Node.
exports.getNode = async (req, res, next) => {

  try {
  
    let data = {}; 

    data.node = await fetch(apiUrl + '/nodes/'  + req.params.id)
      .then(res => res.json())

    data.links = await fetch(apiUrl + '/links?populate=true&node=' + req.params.id)
      .then(res => res.json())

    data.projects = await fetch(apiUrl + '/projects/')
      .then(res => res.json())

    // Get errors from the REST request.
    let errors = data.errors;

    if (errors) {

      // There are errors. Render error messages.
      res.render('error', { title: 'Error', errors: errors, section: '/nodes', user: req.user });

    } else {
      if (data.node.error) {
        res.status(404).render('error', { message: data.node.error, section: '/nodes', user: req.user });
      } else {
        res.render('node', { title: 'Node', links: data.links, node: data.node, projects: data.projects, section: '/nodes', user: req.user } );
      }
    }
  } catch (e) {
    res.render('error', { message: 'An unexpected error ocurred.', section: '/nodes', user: req.user });
  }
};

// Display Node create form on GET.
exports.createNodeGet = async(req, res, next) => {

  try {

    let data = {}; 

    data.projects = await fetch(apiUrl + '/projects/')
      .then(res => res.json())

    // Get errors from the REST request.
    let errors = data.errors;

    if (errors) {

      // There are errors. Render error messages.
      res.render('error', { title: 'Error', errors: errors, section: '/nodes', user: req.user });

    } else {
      res.render('node', { title: 'Create Node', projects: data.projects, section: '/nodes', user: req.user } );
    }
    
  } catch (e) {
    console.log(e); 
  }

}

// Handle Node create on POST.
exports.createNodePost =  [

  // TODO: Validate fields.

  // Sanitize fields.
  sanitizeBody('id').trim().escape(),
  sanitizeBody('group').toInt(),

  // Process request after validation and sanitization.
  async (req, res, next) => {

    try {
  
      let node = { id: req.body.id, group: req.body.group, project: req.body.project };

      let data = await fetch(apiUrl + '/nodes/', { 
        method: 'POST', 
        headers: {
          "Content-Type": "application/json",
          "x-access-token": req.cookies.token 
        },
        body: JSON.stringify(node) }
      )
        .then(res => res.json())

      let projects = await fetch(apiUrl + '/projects/')
        .then(res => res.json())

      // Get server-side validation errors from the REST request.
      let errors = data.errors;

      if (errors) {
        
        // There are errors. Render form again with sanitized values/error messages.
        res.render('node', { title: 'Error', errors: errors, projects: projects, section: '/nodes', user: req.user });

      } else {
        res.render('node', { title: 'Node', message: data.message, node: data.node, projects: projects, section: '/nodes', user: req.user } );
      }

    } catch (e) {
      res.render('error', { message: 'An unexpected error ocurred.', section: '/nodes', user: req.user });
    }
  }
];

// Handle Node update on POST.
exports.updateNode = [

  // TODO: Validate fields.

  // Sanitize fields.
  sanitizeBody('id').trim().escape(),
  sanitizeBody('group').toInt(),

  // Process request after validation and sanitization.
  async (req, res, next) => {

    try {

      let node = { id: req.body.id, group: req.body.group};

      let data = await fetch(apiUrl + '/nodes/' + req.params.id, { 
        method: 'PUT', 
        headers: {
          "Content-Type": "application/json",
          "x-access-token": req.cookies.token
        },
        body: JSON.stringify(node) }
      )
        .then(res => res.json())

      let projects = await fetch(apiUrl + '/projects/')
        .then(res => res.json())

      // Get server-side validation errors from the REST request.
      let errors = data.errors;

      if (errors) {

        // There are errors. Render form again with sanitized values/error messages.
        res.render('node', { title: 'Error', node: node, errors: errors, section: '/nodes', user: req.user });

      } else {
        res.render('node', { title: 'Node', message: data.message, node: data.node, projects: projects, section: '/nodes', user: req.user } );
      }

    } catch (e) {
      res.render('error', { message: 'An unexpected error ocurred.', section: '/nodes', user: req.user });
    }
  }
];

// Display Node delete form on GET.
exports.node_delete_get = async (req, res, next) => {

  try {

    let data = {}; 

    data.node = await fetch(apiUrl + '/nodes/'  + req.params.id)
      .then(res => res.json())

    data.links = await fetch(apiUrl + '/links?populate=true&node=' + req.params.id)
      .then(res => res.json())

    // TODO: Handle errors
  
    // Successful, so render.
    res.render('node_delete', { title: 'Delete Node', links: data.links, node: data.node, section: '/nodes', user: req.user } );

  } catch (e) {
    console.log('nodeController.node_delete_get.e: ' + e);
    res.render('error', { title: 'Error', message: 'An unexpected error ocurred.', section: '/nodes', user: req.user });
  }

};

// Handle Node delete on POST.
exports.node_delete_post = async (req, res, next) => {

  try {
  
    let data = {}; 

    data = await fetch(apiUrl + '/nodes/' + req.params.id, { 
      method: 'DELETE', 
      headers: { 
        'Content-Type': 'application/json',
        'x-access-token': req.cookies.token
      }})
      .then(res => res.json())

    let errors = data.errors;

    if (errors) {
        
      // There are errors. Render the error page with error messages.
      res.render('error', { title: 'Error',  message: errors.message, section: '/nodes', user: req.user } );

    } else {

      // Success - render success page 
      res.render('success', { message: data.message, section: '/nodes', user: req.user } );
    }

  } catch (e) {
    res.render('error', { message: 'An unexpected error ocurred.', section: '/nodes', user: req.user });
  }

};

// Display Nodes upload form on GET.
exports.uploadNodesGet = function (req, res, next) {
  res.render('nodes_upload', { title: 'Upload Nodes', section: '/nodes', user: req.user } );
};

// Handle Nodes upload on POST.
exports.uploadNodesPost = async (req, res, next) => {

  try {

    let formData = new FormData();
    formData.append('file', req.file.buffer, req.file.originalname );  

    let data = await fetch(apiUrl + '/nodes/upload', { 
      method: 'POST', 
      headers: {
        'x-access-token': req.cookies.token 
      },
      body: formData }
    )

    let response = await data.json();
    res.render('nodes_upload', { title: 'Upload Nodes', message: response.message, section: '/nodes', user: req.user } );

  } catch (e) {
    console.log(e); 
    res.render('error', { message: 'An unexpected error ocurred.', section: '/nodes', user: req.user });
  }

};

