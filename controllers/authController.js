const { body,validationResult } = require('express-validator/check');
const config = require('config');
const fetch = require("node-fetch");
const jwt = require('jsonwebtoken');
const mongoose = require("mongoose");
const { sanitizeBody } = require('express-validator/filter');
const User = require("../models/user");

const apiUrl = config.API_URL;


// Go to index page
exports.index = function(req, res, next) {
  res.render('index', { title: 'Networks', section: '/', user : req.user });
};

// Go to registration page
exports.register_get = function(req, res, next) {
  res.render('register');
};

// Post registration
exports.register_post = [

  // Validate field input
  body('email')
    .isLength({ min: 1 }).trim().withMessage('Email is required'),

  body('password')
    .isLength({ min: 1 }).trim().withMessage('Password is required'),

  // Sanitize fields.
  sanitizeBody('email').trim().escape(),
  sanitizeBody('password').trim().escape(),

  // Process request after validation and sanitization.
  async (req, res, next) => {

    try {

      let confObj = { method: 'POST', headers: { "Content-Type": "application/json" }, body: JSON.stringify(req.body) }
      let data = await fetch(apiUrl + '/sign-up', confObj)
        .then(res => res.json())

      // Get server-side validation errors from the REST request.
      let errors = data.errors;
  
      if (errors) {

        // There are errors. Render form again with sanitized values/error messages.
        res.render('register', { title: 'Error', errors: errors });

      } else { 

        res.render('user', { title: 'User', message: data.message, user: data.user } );

      }

    } catch (e) {
      console.log(e);
      res.render('error', { message: 'An unexpected error ocurred.', user: req.user });
    }
  }
];

// Go to login page
exports.login_get = function(req, res) {
  res.render('login', {redirect: req.query.redirect});
};

// Post login
exports.login_post = [

  // Validate field input
  body('email')
    .isLength({ min: 1 }).trim().withMessage('Email is required'),

  body('password')
    .isLength({ min: 1 }).trim().withMessage('Password is required'),

  // Sanitize fields.
  sanitizeBody('email').trim().escape(),
  sanitizeBody('password').trim().escape(),

  // Process request after validation and sanitization.
  async (req, res, next) => {

    try {

      let confObj = { method: 'POST', headers: { "Content-Type": "application/json" }, body: JSON.stringify(req.body) }
      let data = await fetch(apiUrl + '/sign-in', confObj)
        .then(res => res.json())

      // Get server-side validation errors from the REST request.
      let errors = data.errors;
  
      if (errors) {

        // There are errors. Render form again with sanitized values/error messages.
        res.render('login', { title: 'Error', errors: errors });

      } else {

        let redirect = req.body.redirect;  

        res.cookie('token', data.token, { maxAge: 86400 });
 
        if (redirect) {

          res.redirect(redirect); 

        } else {

          res.redirect('/account'); 

        }
      }
    } catch (e) {
      console.log(e);
      res.render('error', { message: 'An unexpected error ocurred.', user: req.user });
    }
  } 
];

// Logout
exports.logout = function(req, res) {
  req.logout();
  res.clearCookie('token')
    .redirect('/');
};

// Go to account page
exports.account_get = function(req, res) {
  res.render('account', { section: '/account', title: 'User', user: req.user });
};

