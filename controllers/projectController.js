const { body,validationResult } = require('express-validator/check');
const config = require('config');
const fetch = require('node-fetch');
const FormData = require('form-data');
const Project = require('../models/project');
const { sanitizeBody } = require('express-validator/filter');

const apiUrl = config.API_URL;


// Display paginated list of Projects.
exports.project_list = async (req, res, next) => {

  let page = 1; 
  if (parseInt(req.query.page, 10)) {
    page = parseInt(req.query.page, 10); 
  };

  try {

    let data = await fetch(apiUrl + '/projects/' + '?page=' + page)
      .then(res => res.json())

    res.render('projects', { title: 'Network Projects', data: data, section: '/projects', user: req.user });

  } catch (e) {
    res.render('error', { message: 'An unexpected error ocurred.', section: '/projects', user: req.user });
  }
};


// Display detail page for a specific Project.
exports.project_detail = async (req, res, next) => {

  try {
  
    let data = {}; 

    data.project = await fetch(apiUrl + '/projects/'  + req.params.id)
      .then(res => res.json())

    // Get errors from the REST request.
    let errors = data.errors;

    if (errors) {

      // There are errors. Render error messages.
      res.render('error', { title: 'Error', errors: errors, section: '/projects', user: req.user });

    } else {
      if (data.project.error) {
        res.status(404).render('error', { message: data.project.error, section: '/projects', user: req.user });
      } else {
        res.render('project', { title: 'Project', project: data.project, section: '/projects', user: req.user } );
      }
    }
  } catch (e) {
    res.render('error', { message: 'An unexpected error ocurred.', section: '/projects', user: req.user });
  }
};

// Display Project create form on GET.
exports.project_create_get = function(req, res, next) {
  res.render('project', { title: 'New Project', section: '/projects', user: req.user } );
};

// Handle Project create on POST.
exports.project_create_post =  [

  // TODO: Validate fields.

  // Sanitize fields.
  sanitizeBody('title').trim().escape(),
  sanitizeBody('description').trim().escape(),

  // Process request after validation and sanitization.
  async (req, res, next) => {

    try {

      let project = { title: req.body.title, description: req.body.description };

      let data = await fetch(apiUrl + '/projects/', { 
        method: 'POST', 
        headers: {
          "Content-Type": "application/json",
          "x-access-token": req.cookies.token 
        },
        body: JSON.stringify(project) }
      )
        .then(res => res.json())

      // Get server-side validation errors from the REST request.
      let errors = data.errors;

      if (errors) {
        
        // There are errors. Render form again with sanitized values/error messages.
        res.render('project', { title: 'Error', errors: errors, section: '/projects', user: req.user });

      } else {
        res.render('project', { title: 'Project', message: data.message, project: data.project, section: '/projects', user: req.user } );
      }

    } catch (e) {
      res.render('error', { message: 'An unexpected error ocurred.', section: '/projects', user: req.user });
    }
  }
];

// Handle Project update on POST.
exports.project_update_post = [

  // TODO: Validate fields.

  // Sanitize fields.
  sanitizeBody('title').trim().escape(),
  sanitizeBody('description').trim().escape(),

  // Process request after validation and sanitization.
  async (req, res, next) => {

    try {
  
      let project = { title: req.body.title, description: req.body.description };

      let data = await fetch(apiUrl + '/projects/' + req.params.id, { 
        method: 'PUT', 
        headers: {
          "Content-Type": "application/json",
          "x-access-token": req.cookies.token 
        },
        body: JSON.stringify(project) }
      )
        .then(res => res.json())

      // Get server-side validation errors from the REST request.
      let errors = data.errors;

      if (errors) {

        // There are errors. Render form again with sanitized values/error messages.
        res.render('project', { title: 'Error', project: project, errors: errors, section: '/projects', user: req.user });

      } else {
        res.render('project', { title: 'Edit Project', message: data.message, project: data.project, section: '/projects', user: req.user } );
      }

    } catch (e) {
      res.render('error', { message: 'An unexpected error ocurred.', section: '/projects', user: req.user });
    }
  }
];

// Display Project delete form on GET.
exports.project_delete_get = async (req, res, next) => {

  try {

    let data = {}; 

    data.project = await fetch(apiUrl + '/projects/'  + req.params.id)
      .then(res => res.json())

//    data.nodes = await fetch(apiUrl + '/nodes?populate=true&project=' + req.params.id)
//      .then(res => res.json())

    // TODO: Handle errors
  
    // Successful, so render.
    res.render('project_delete', { title: 'Delete Project',  project: data.project, section: '/projects', user: req.user } );
//    res.render('project_delete', { title: 'Delete Project', nodes: data.nodes, project: data.project, section: '/projects', user: req.user } );

  } catch (e) {
    res.render('error', { title: 'Error', message: 'An unexpected error ocurred.', section: '/projects', user: req.user });
  }

};

// Handle Project delete on POST.
exports.project_delete_post = async (req, res, next) => {

  try {
  
    let data = {}; 

    data = await fetch(apiUrl + '/projects/' + req.params.id, { 
      method: 'DELETE', 
      headers: { 
        'Content-Type': 'application/json',
        'x-access-token': req.cookies.token
      }})
      .then(res => res.json())

    let errors = data.errors;

    if (errors) {
        
      // There are errors. Render the error page with error messages.
      res.render('error', { title: 'Error',  message: errors.message, section: '/projects', user: req.user } );

    } else {

      // Success - render success page 
      res.render('success', { message: data.message, section: '/projects', user: req.user } );
    }

  } catch (e) {
    res.render('error', { message: 'An unexpected error ocurred.', section: '/projects', user: req.user });
  }

};

// Display Projects upload form on GET.
exports.uploadProjectsGet = function (req, res, next) {
  res.render('projects_upload', { title: 'Upload Projects', section: '/projects', user: req.user } );
};

// Handle Projects upload on POST.
exports.uploadProjectsPost = async (req, res, next) => {

  try {

    let formData = new FormData();
    formData.append('file', req.file.buffer, req.file.originalname );

    let data = await fetch(apiUrl + '/projects/upload', {
      method: 'POST',
      headers: {
        'x-access-token': req.cookies.token
      },
      body: formData }
    )

    let response = await data.json(); 
    res.render('projects_upload', { title: 'Upload Projects', message: response.message, section: '/projects', user: req.user } );

  } catch (e) {
    console.log(e);
    res.render('error', { message: 'An unexpected error ocurred.', section: '/projects', user: req.user });
  }

};

