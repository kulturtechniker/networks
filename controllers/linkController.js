const { body,validationResult } = require('express-validator/check');
const config = require('config');
const fetch = require("node-fetch");
const FormData = require('form-data');
const Link = require('../models/link');
const { sanitizeBody } = require('express-validator/filter');

const apiUrl = config.API_URL;


// Display paginated list of Links.
exports.link_list = async (req, res, next) => {

  let page = 1;
  if (parseInt(req.query.page, 10)) {
    page = parseInt(req.query.page, 10);
  };

  try {

    let data = await fetch(apiUrl + '/links' + '?populate=true&page=' + page)
      .then(res => res.json())

    res.render('links', { title: 'Network Links', data: data, section: '/links', user: req.user });

  } catch (e) {
    res.render('error', { message: 'An unexpected error ocurred.', section: '/links', user: req.user });
  }
};


// Display detail page for a specific Link.
exports.link_detail = async (req, res, next) => {

  try {

    let data = {}; 

    data.link = await fetch(apiUrl + '/links/' + req.params.id)
      .then(res => res.json())

    data.nodes = await fetch(apiUrl + '/nodes/')
      .then(res => res.json())

    // Get errors from the REST request.
    let errors = data.errors;

    if (errors) {

      // There are errors. Render form again with sanitized values/error messages.
      res.render('error', { title: 'Error', errors: errors, section: '/links', user: req.user });

    } else {
      if (data.link.error) {
        res.status(404).render('error', { message: data.link.error, section: '/links', user: req.user });
      } else {
        res.render('link', { title: 'Link', link: data.link, nodes: data.nodes, section: '/links', user: req.user } );
      }
    }
  } catch (e) {
    res.render('error', { message: 'An unexpected error ocurred.', section: '/links', user: req.user });
  }
};

// Display Link create form on GET.
exports.link_create_get = async (req, res, next) => {

  try {

    let nodes = await fetch(apiUrl + '/nodes/')
      .then(res => res.json())

    // Get errors from the REST request.
    let errors = nodes.errors;

    if (errors) {

      // There are errors. Render form again with sanitized values/error messages.
      res.render('error', { title: 'Error', errors: errors, section: '/links', user: req.user });

    } else {
      res.render('link', {title: 'Create Link', nodes: nodes, section: '/links', user: req.user } );
    }
  } catch (e) {
    console.log(e);
    res.render('error', { message: 'An unexpected error ocurred.', section: '/links', user: req.user });
  }

  // res.render('link', { title: 'New Link', section: '/links', user: req.user });
};

// Handle Link create on POST.
exports.link_create_post =  [

  // TODO: Validate fields.
    
  // Sanitize fields.
  sanitizeBody('source').trim().escape(),
  sanitizeBody('target').trim().escape(),
  sanitizeBody('value').toInt(),

  // Process request after validation and sanitization.
  async (req, res, next) => {

    try {

      let link = { source: req.body.source, target: req.body.target, value: req.body.value };

      let data = await fetch(apiUrl + '/links/', {
        method: 'POST', 
        headers: {
          'Content-Type': 'application/json',
          'x-access-token': req.cookies.token
        },
        body: JSON.stringify(link) }
      )
        .then(res => res.json())

      let nodes = await fetch(apiUrl + '/nodes/')
        .then(res => res.json())
        
      // Get server-side validation errors from the REST request.
      let errors = data.errors;

      if (errors) {
         
        // There are errors. Render form again with sanitized values/error messages.
        res.render('link', { title: 'Error', link: link, errors: errors, section: '/links', user: req.user });

      } else {
        res.render('link', { title: 'Edit Link', message: data.message, link: data.link, nodes: nodes, section: '/links', user: req.user } );
      } 

    } catch (e) {
      res.render('error', { message: 'An unexpected error ocurred.', section: '/links', user: req.user });
    }
  }
];

// Handle Link update on POST.
exports.link_update_post = [

  // TODO: Validate fields.
//  body('source', 'Source must not be empty.').isLength({ min: 1 }).trim(),
//  body('target', 'Target must not be empty.').isLength({ min: 1 }).trim(),
//  body('value', 'Value must not be empty.').isLength({ min: 1 }).trim(),

  // Sanitize fields.
  sanitizeBody('source').trim().escape(),
  sanitizeBody('target').trim().escape(),
  sanitizeBody('value').trim().escape(),

  // Process request after validation and sanitization.
  async (req, res, next) => {

    try {

      let link = { source: req.body.source, target: req.body.target, value: req.body.value }

      console.log('link = ' + JSON.stringify(link)); 

      let data = await fetch(apiUrl + '/links/' + req.params.id, {
        method: 'PUT',
        headers: {
          "Content-Type": "application/json",
          "x-access-token": req.cookies.token
        },
        body: JSON.stringify(link) }
      )
        .then(res => res.json())

      let nodes = await fetch(apiUrl + '/nodes/')
        .then(res => res.json())

      // Get server-side validation errors from the REST request.
      let errors = data.errors;

      if (errors) {

        // There are errors. Render form again with sanitized values/error messages.
        res.render('link', { title: 'Error', errors: errors, section: '/links', user: req.user });

      } else {
        res.render('link', { title: 'Edit Link', message: data.message, link: data.link, nodes: nodes, section: '/links', user: req.user } );
      }

    } catch (e) {
      res.render('error', { message: 'An unexpected error ocurred.', section: '/links', user: req.user });
    }
  }
];

// Display Link delete form on GET.
exports.link_delete_get = async (req, res, next) => {

  try {
    const data = await fetch(apiUrl + '/links/' + req.params.id)
      .then(res => res.json())

    // Successful, so render.
    res.render('link_delete', { title: 'Delete Link', link: data, section: '/links', user: req.user } );

  } catch (e) {
    console.log(e);
    res.render('error', { title: 'Error', message: 'An unexpected error ocurred.', section: '/links', user: req.user });
  }

};

// Handle Link delete on POST.
exports.link_delete_post = async (req, res, next) => {

  try {

    let data = await fetch(apiUrl + '/links/' + req.params.id, { 
      method: 'DELETE', 
      headers: { 
        'Content-Type': 'application/json',
        'x-access-token': req.cookies.token 
      }})
      .then(res => res.json())

    let errors = data.errors;

    if (errors) {

      // There are errors. Render the delete form again with error messages.
      res.render('link_delete', { title: 'Error', errors: errors, section: '/links', user: req.user });

    } else {

      // Success - render success page 
      res.render('success', { message: data.message, section: '/links', user: req.user } );
    }

  } catch (e) {
    res.render('error', { message: 'An unexpected error ocurred.', section: '/links', user: req.user });
  }
  
};

// Display Links upload form on GET.
exports.uploadLinksGet = function (req, res, next) {
  res.render('links_upload', { title: 'Upload Links', section: '/links', user: req.user } );
};

// Handle Links upload on POST.
exports.uploadLinksPost = async (req, res, next) => {

  try {

    let formData = new FormData();
    formData.append('file', req.file.buffer, req.file.originalname );

    let data = await fetch(apiUrl + '/links/upload', {
      method: 'POST',
      headers: {
        'x-access-token': req.cookies.token
      },
      body: formData }
    )
 
    let response = await data.json();
    res.render('links_upload', { title: 'Upload Links', message: response.message, section: '/links', user: req.user } );

  } catch (e) {
    console.log(e);
    res.render('error', { message: 'An unexpected error ocurred.', section: '/links', user: req.user });
  }

};

