const cookieParser = require('cookie-parser');
const createError = require('http-errors');
const express = require('express');
const favicon = require('serve-favicon')
const path = require('path');
const LocalStrategy = require('passport-local').Strategy;
const logger = require('morgan');
const passport = require('passport'); 
const sassMiddleware = require('node-sass-middleware');

const apiRouter = require('./routes/api');
const indexRouter = require('./routes/index');
const linksRouter = require('./routes/links');
const nodesRouter = require('./routes/nodes');
const projectsRouter = require('./routes/projects');
const usersRouter = require('./routes/users');

// Require the model with Passport-Local Mongoose plugged in
const User = require('./models/user');

const app = express();

const config = require('config'); // We load the db location from the JSON files

// Set up mongoose connection
const mongoose = require('mongoose');
mongoose.connect(config.DBHost, { useNewUrlParser: true });
mongoose.Promise = global.Promise;

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(favicon(path.join(__dirname, 'public/images', 'favicon.ico')))

// Don't show the log when it is test
if(config.util.getEnv('NODE_ENV') !== 'test') {
  app.use(logger('dev'));
  // app.use(logger('combined')); //'combined' outputs the Apache style LOGs
}

// Initialize passport and express-session
app.use(require('express-session')({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());

passport.use(User.createStrategy());

// Use static serialize and deserialize of model for passport session support
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: true, // true = .sass and false = .scss
  sourceMap: true
}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/api', apiRouter);
app.use('/links', linksRouter);
app.use('/nodes', nodesRouter);
app.use('/projects', projectsRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

